#N11
lang = ['Python','Golang','C++','JavaScript','Ruby','Swift','C','Java','PHP','Rust']
print(f'Os três primeiros itens da minha lista são {lang[:3]}')

print(f'Os três últimos itens da lista são {lang[-3:]}')

print(f'Os três itens do meio são {lang[3:6]}')
#N12
pizza_hut = ['Quatro Queijos','Margherita','Pepperoni','Frango com Catupiry']
pizza_hot_paraguai = pizza_hut[:]

pizza_hot_paraguai.append('Cebola')

print(f'Pizza Hut: {pizza_hut}')
print(f'Pizza Hot do Paraguai: {pizza_hot_paraguai}')
#N13
pizza_hut = ['Quatro Queijos','Margherita','Pepperoni','Frango com Catupiry']
pizza_hot_paraguai = pizza_hut[:]
price = ['16,00','15,00','18,00','15,00']

pizza_hot_paraguai.append('Cebola')
price.append('900,00')

print('PIZZA HUT')
for pos in range(1, 5):
        print(f'{pos} - {pizza_hut[pos-1]} - {price[pos-1]}')

print('')

print('PIZZA HOT')
for pos in range(1, 6):
        print(f'{pos} - {pizza_hot_paraguai[pos-1]} - {price[pos-1]}')
