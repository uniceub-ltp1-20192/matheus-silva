#7.1
convidados = ["BOB Marley","Thiago ventura","Michek b jordan","Ronaldo"]
for convidado in convidados:
    print(convidado + ", você está convidado para o jantar")
print("")

#7.2
convidados.append("Thiaguinho")
naoPodeComparecer = convidados.pop(0)
print(naoPodeComparecer," não pode mais comparecer ao jantar")
for convidado in convidados:
    print(convidado + ", você está convidado para o jantar ")
print("")

#7.3
print("Encontrei uma mesa de jantar maior!")
convidados.insert(0,"Kevin Hart")
convidados.insert(len(convidados)//2,"Lebron James")
convidados.append("Robert Downey Jr")
for convidado in convidados:
    print(convidado + ", você está convidado")
print("")

#7.4
print("Aconteceu um imprevisto e agora só posso convidar duas pessoas")
while len(convidados) > 2:
    removido = convidados.pop()
    print(removido + ", me desculpe")

for convidado in convidados:
    print(convidado + ", você ainda está convidado.")

for i in range(len(convidados)):
    del convidados[0]
print("LISTA:",convidados)
