#6
nomes = ["Diego","Luis","Marcos","Caio","Pedro"]
for nome in nomes:
    print(nome)
print("")

#6.2
for nome in nomes:
    print(nome," é meu amigo")
print("")

#6.3
carros = ["Audi","BMW","Mercedes","Ferrari"]
for carro in carros:
    print("Eu gostaria de ter uma",carro)
