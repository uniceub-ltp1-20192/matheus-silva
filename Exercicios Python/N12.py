
PRATOS = ("PIZZA","CHURRASCO","LASANHA","HAMBURGUER","FRANGO")
MIN_VALUE = 1
MAX_VALUE = 1

def apresentaMenu():
    comidas = []
    for prato in PRATOS:
        for value in range(MIN_VALUE, MAX_VALUE + 1):
            comida = (prato)
            comidas.append(comida)
    return comidas

print(apresentaMenu())