num_grades = 0
sum_grades = 0
sum_weighted_grades = 0
sum_weighted = 0
continuar = 0


def media(x, y):
    x = x + y
    return x


def mediaPonterada(a, b, c):
    a = a + (b * c)
    return a


while continuar == 0:
    num_grades = num_grades + 1

    while True:
        try:
            grade = float(input("Informe a nota (0-10): "))
            if not 0 <= grade <= 10:
                raise ValueError("Nota fora do padrão permitido")
        except ValueError as e:
            print("Valor inválido:", e)
        else:
            break

    while True:
        try:
            weight = int(input("Peso (1-5): "))
            if not 0 <= weight <= 5:
                raise ValueError("Peso fora do padrão permitido")
        except ValueError as e:
            print("Valor inválido:", e)
        else:
            break

    sum_grades = media(sum_grades, grade)
    sum_weighted_grades = mediaPonterada(sum_weighted_grades, grade, weight)
    sum_weighted = sum_weighted + weight

    while True:
        try:
            aux = str(input("Deseja continuar? S/N "))
            if not aux == 's' and aux == 'S' and aux == 'n' and aux == 'N':
                raise ValueError("Resposta invalida !!")
        except ValueError as e:
            print("Valor inválido:", e)
        else:
            break

    if aux == 'n' or aux == 'N':
        continuar = 1
    if aux == 's' or aux == 'S':
        continuar = 0

print("\n")
print("-----------------------------------------------")
print("Média: ", sum_grades / num_grades)
print("Média ponderada: ", sum_weighted_grades / sum_weighted)
